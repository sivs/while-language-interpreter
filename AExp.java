public class AExp
{
  private final String exp;
  private final State s;
  public AExp(String exp, State s)
  {
   this.exp = exp;
   this.s   = s;
  }

  private boolean isNumber(String str)
  {
    try
    {
      Integer.parseInt(str);
    }
    catch(NumberFormatException e)
    {
      return false;
    }
    catch (NullPointerException e)
    {
      return false;
    }
    return true;
  }


  public int getValue()
  {
    // Check if it is a number 
    if(isNumber(exp))
    {
      return Integer.parseInt(exp);
    }
    // Check if it has any additions
    else if(exp.indexOf('+') != -1)
    {
      int splitIndex = exp.indexOf('+');
      String left = exp.substring(0 ,splitIndex);
      String right = exp.substring(splitIndex + 1, exp.length());

      int res = new AExp(left,s).getValue() + new AExp(right,s).getValue();
      System.out.println(left + " + " + right + " = " + res);
      return res;
    }
    // Check if it has any subtractions
    else if(exp.indexOf('-') != -1)
    {
      int splitIndex = exp.indexOf('-');
      String left = exp.substring(0 ,splitIndex);
      String right = exp.substring(splitIndex + 1, exp.length());

      int res = new AExp(left,s).getValue() - new AExp(right,s).getValue();
      System.out.println(left + " - " + right + " = " + res);
      return res;
    }
    // Check if it has any multiplications
    else if(exp.indexOf('*') != -1)
    {
      int splitIndex = exp.indexOf('*');
      String left = exp.substring(0 ,splitIndex);
      String right = exp.substring(splitIndex + 1, exp.length());

      int res = new AExp(left,s).getValue() * new AExp(right,s).getValue();
      System.out.println(left + " * " + right + " = " + res);
      return res;
    }
    // It must be a variable name
    else
    {
      System.out.println("Looking up: " + exp);
      return s.get(exp);
    }
  }
  
  public static void main(String[] args)
  {
    System.out.println(
        "This is the Arithmetic expression class for my while interpreter");
    State s = new State();
    s.set("Sivert", 20);
    System.out.println("" + new AExp("1+2*3-1*5+Sivert", s).getValue());
  }
}
