public class BExp
{
  private final String exp;
  private final State s;
  public BExp(String exp, State s)
  {
    this.exp = exp;
    this.s   = s;
  }

  public boolean getValue()
  {
    // Check if it is just true or false
    if(exp.equals("true"))    return true;
    else if(exp.equals("false")) return false;
    // AND operator 
    else if(exp.indexOf("AND") != -1)
    {
      int splitIndex = exp.indexOf("AND");
      String left = exp.substring(0,splitIndex);
      String right = exp.substring(splitIndex + 3, exp.length());

      boolean res = new BExp(left, s).getValue() == new BExp(right,s).getValue();
      System.out.println(left + " AND " + right + " is " + (res ? "true":"false"));
      return res;
    }
    // Negation
    else if(exp.charAt(0) == '!')
    {
      String right = exp.substring(1, exp.length());

      boolean res = ! new BExp(right,s).getValue();
      System.out.println("Negation of " + right + " is " + res);
      return res;
    }
    // If equality of two AExps
    else if(exp.indexOf("==") != -1)
    {
      int splitIndex = exp.indexOf("==");
      String left = exp.substring(0,splitIndex);
      String right = exp.substring(splitIndex + 2, exp.length());

      boolean res = new AExp(left, s).getValue() == new AExp(right,s).getValue();
      System.out.println(left + " == " + right + " is " + (res ? "true":"false"));
      return res;
    }
    else if(exp.indexOf("<=") != -1)
    {
      int splitIndex = exp.indexOf("<=");
      String left = exp.substring(0,splitIndex);
      String right = exp.substring(splitIndex + 2, exp.length());

      boolean res = new AExp(left, s).getValue() <= new AExp(right,s).getValue();
      System.out.println(left + " <= " + right + " is " + (res ? "true":"false"));
      return res;
    }
    return false;
  }
  public static void main(String[] args)
  {
    System.out.println(
        "This is the boolean expression evaluator "
       +"for my while language interpreter");
    State s = new State();
    s.set("Sivert", 1336);
    System.out.println(new BExp("!falseANDSivert<=1337", s).getValue());
  }
}
