import java.util.HashMap;
import java.util.Map;

public class State 
{
  private final HashMap<String, Integer> varMap;
  private boolean debug = false;

  public State()
  {
    varMap = new HashMap<String, Integer>(); 
  }

  public void activateDebug(){debug = true;}
  public void disableDebug(){debug = false;}

  public void set(String identifier, int value)
  {
    // Check if the variable is already set
    // but overwrite anyway because we don't support final variables
    if(varMap.containsKey(identifier))
    {
      varMap.put(identifier,value);
    }
    else
    {
      varMap.put(identifier,value);
    }
  }

  public int get(String identifier)
  {
    // we have to return 0 if it is not set
    if(varMap.containsKey(identifier))
    {
      return varMap.get(identifier);
    }
    else
    {
      return 0;
    }
  }

  // Print all variable mappings for debugging purposes
  public String toString()
  {
    // Get all the identifiers
    String stringRep = "";
    for(String identifier: varMap.keySet().toArray(new String[0]))
    {
      stringRep += (identifier + " => " + varMap.get(identifier) + "\n");
    }
    return stringRep;
  }

  public static void main(String[] args)
  {
    System.out.println("This is the state class for my while interpreter.");
    State s = new State();
    s.set("Sivert", 1337);
    s.set("Robert", 199234);
    s.set("Connor", 89393);
    System.out.println("" + s);
  }
}
