import java.io.*;
import java.util.Scanner;

public class While
{
  private final String S;
  private final State  s;

  public While(String S, State s)
  {
    this.S = S;
    this.s = s;
  }

  private int findEndOfBlock(String subExp, int headerStart)
  {
    // headerStart is the index of the first (
    // return the index of the last )
    int depth = 0;
    int i     = headerStart;
    do
    {
      char curChar = subExp.charAt(i);
      switch (curChar)
      {
        case '(': depth++; break;
        case ')': depth--; break;
        default :          break;

      }
      i++;
    } while(depth > 0);

    return i;
  }

  public int getSplitIndex(String subExp)
  {
    int depth = 0;
    int i = 0;
    while(i < subExp.length())
    {
      char curChar = subExp.charAt(i);
      switch (curChar)
      {
        case '(': depth++; break;
        case ')': depth--; break;
        default :          break;
      }

      if(curChar == ';' && depth == 0) return i;
      i++;
    }
    return -1;
  }

  // Will mutate the original state as well as returning it
  public State eval()
  {
    // skip
    if(S.equals("skip")) return s;
    // composition
    //else if(S.indexOf(";") != -1)
    else if(getSplitIndex(S) != -1)
    {
      int splitIndex = getSplitIndex(S); 
      String left   = S.substring(0,splitIndex);
      String right  = S.substring(splitIndex + 1, S.length());

      // Execute left side, then right side
      new While(left, s).eval();
      new While(right, s).eval();
    }
    // if else
    else if(S.length() > 2 && S.substring(0,2).equals("if"))
    {
      // Find the if header and if bodys
      // use () parantheses for the header and {} for the body
      int headerStart = 2;
      int headerEnd = findEndOfBlock(S,2);

      //System.out.println("Header ends at: " + headerEnd);

      //System.out.println("Trying to use the if rule on string: " + S);
      // Subtract one from headerEnd because it is the index of the closing
      // paranthesis, not the last character in the code
      String header = S.substring(headerStart + 1, headerEnd - 1);

      // Find the first if body
      //System.out.println("header: " + header);
      // )(
      
      // Search for the first parantheses from the end of the header
      int body1Start = S.indexOf('(', headerEnd);
      int body1End   = findEndOfBlock(S,body1Start);
      //System.out.println("Body1 start/end: " + body1Start + "/" + body1End);
      String body1   = S.substring(body1Start + 1, body1End - 1);

      //System.out.println("Body1: " + body1);

      // Find the second if body
      int body2Start = S.indexOf('(', body1End);
      int body2End   = findEndOfBlock(S,body2Start);
      //System.out.println("Body2 start/end: " + body2Start + "/" + body2End);
      String body2   = S.substring(body2Start + 1, body2End - 1);

      //System.out.println("Body2: " + body2);
     
      // Evaluate the header to determine which body to execute
      if(new BExp(header, s).getValue()) // Execute body1
        new While(body1, s).eval();
      else                               // Execute body2
        new While(body2, s).eval();

    }
    // while
    else if(S.length() > 4 && S.substring(0,5).equals("while"))
    {
      // find the while header and body
      // use () parantheses for the header and {} for the body
      int headerStart = 5;
      int headerEnd = findEndOfBlock(S,headerStart);

      //System.out.println("Header ends at: " + headerEnd);

      //System.out.println("Trying to use the while rule on string: " + S);
      // Subtract one from headerEnd because it is the index of the closing
      // paranthesis, not the last character in the code
      String header = S.substring(headerStart + 1, headerEnd - 1);

      // Search for the first parantheses from the end of the header
      int bodyStart = S.indexOf('(', headerEnd);
      int bodyEnd   = findEndOfBlock(S,bodyStart);

      //System.out.println("Body start/end: " + bodyStart + "/" + bodyEnd);
      String body   = S.substring(bodyStart + 1, bodyEnd - 1);

      //System.out.println("While body: " + body);

      if(new BExp(header, s).getValue())  
      {
        // Run loop body
        new While(body, s).eval();
        // Run loop again recursivly
        new While(S,s).eval();;
      }
    }
    // Assignment
    else if(S.indexOf(":=") != -1)
    {
      int splitIndex = S.indexOf(":=");
      String identifier = S.substring(0, splitIndex);
      String expression = S.substring(splitIndex + 2, S.length());
      s.set(identifier, new AExp(expression, s).getValue());
    }
    return s; 
  }

  public static void main(String[] args)
  {

    System.out.println("While interpreter v0.2");

    State myState = new State();
    String input;
    BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    try
    {
    while((input = reader.readLine()) != null)
    {
      System.out.println(input);

      String[] splitedInput = input.split("\\s+");
      if(splitedInput[0].equals(":Q"))
      {
        // Exit program
        break;
      }
      if(splitedInput[0].equals(":H"))
      {
        // Show help text
        System.out.println("Available options are: :Q - Quit...");
      }
      else if(splitedInput[0].equals(":L"))
      {
       // Load code from file and execute it
       // Dirty one liner to read a file into a String, fails on empty file
       String code = new Scanner( new File(splitedInput[1]), "UTF-8" ).useDelimiter("\\A").next();
       // Execute code
       new While(code, myState).eval();
      }
      else if(splitedInput[0].equals(":S"))
      {
       // Print the current state
       System.out.println("Printing current state: ");
       System.out.print("" + myState);
      }
      else
      {
       // Interpret input as while code
       new While(input, myState).eval();
      }
    }
    }
    catch(IOException e)
    {
      System.out.println(e); 
    }
  }
}
